const { Server } = require('ws')
const ws = new Server({ port: 8081 })
const messages = ['ready!']
const sockets = new Set()
ws.on('connection', socket => {
  console.log('connected!')
  sockets.add(socket)
  socket.send(`<div id="chat">${messages.join('<br>')}</div>`)
  socket.on('message', ms => {
    const obj = JSON.parse(ms.toString())
    messages.push(
      `${obj.user}: ${obj.input}`.replace(
        /[&<>]/g,
        m => ({ '&': '&amp;', '<': '&lt;', '>': '&gt;' }[m]),
      ),
    )
    for (const socket of sockets)
      socket.send(`<div id="chat">${messages.join('<br>')}</div>`)
  })
  socket.on('close', () => {
    console.log('good bye.')
    sockets.delete(socket)
  })
})
